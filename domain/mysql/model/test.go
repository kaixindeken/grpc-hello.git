package model

type Test struct {
	ID    int64  `gorm:"primary_key;not_null;auto_increment"`
	value string `json:"value"`
}
