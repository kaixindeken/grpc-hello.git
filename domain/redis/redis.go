package redis

import (
	"errors"
	"fmt"
	"github.com/gomodule/redigo/redis"
	"strconv"
	"time"
)

const (
	AddrInternalProd = "127.0.0.1:6379"
	AddrExternalProd = "127.0.0.1:6379"
	AddrInternalTest = "127.0.0.1:6379"
	AddrExternalTest = "127.0.0.1:6379"
	Password         = "123456"
)

type RedisConn redis.Conn

type BaseClient struct {
	redisPool *redis.Pool
	address   string
}

func getRedisPool(host string) *redis.Pool {
	return &redis.Pool{
		MaxIdle:     50,  //最大空闲连接数，没有redis操作进依然可以保持这个连接数量
		MaxActive:   400, //连接池最大连接数量,不确定可以用0（0表示自动定义），按需分配
		Wait:        true,
		IdleTimeout: 120 * time.Second, //空闲连接关闭时间

		Dial: func() (redis.Conn, error) {
			option := redis.DialPassword(Password)
			c, err := redis.Dial("tcp", host, option)
			if err != nil {
				return nil, errors.New(err.Error() + host + Password)
			}
			if _, err := c.Do("ping"); err != nil {
				c.Close()
				return nil, err
			}
			return c, err
		},
		TestOnBorrow: func(c redis.Conn, t time.Time) error { //空闲连接状态检查
			_, err := c.Do("PING")
			if err != nil {
				return err
			}
			return nil
		},
	}

}

func (c *BaseClient) Lock(key, value string, ttl int64) (bool, error) {
	conn := c.redisPool.Get()
	defer conn.Close()
	_, err := redis.String(conn.Do("SET", key, value, "PX", ttl, "NX"))
	if err == redis.ErrNil {
		return false, nil
	}
	if err != nil {
		return false, err
	}
	return true, nil
}

func (c *BaseClient) Unlock(key string) error {
	conn := c.redisPool.Get()
	defer conn.Close()
	_, err := conn.Do("del", key)
	return err
}

func (c *BaseClient) GetConn() redis.Conn {
	return c.redisPool.Get()
}

//设置键值的过期时间
func (c *BaseClient) PEXPIRE(key string, ttl int64) (bool, error) {
	conn := c.redisPool.Get()
	defer conn.Close()
	return redis.Bool(conn.Do("PEXPIRE", key, ttl))
}

//设置key的过期时间戳 秒
func (c *BaseClient) EXPIREAT(key string, ttl int64) (int, error) {
	conn := c.redisPool.Get()
	defer conn.Close()
	return redis.Int(conn.Do("EXPIREAT", key, ttl))
}

//------------字符串的操作------------
// 判断所在的 key 是否存在
func (c *BaseClient) Exist(name string) (bool, error) {
	conn := c.redisPool.Get()
	defer conn.Close()
	return redis.Bool(conn.Do("EXISTS", name))
}

// 删除键值
func (c *BaseClient) DelKey(key string) error {
	conn := c.redisPool.Get()
	defer conn.Close()
	_, err := conn.Do("DEL", key)
	return err
}

//设置键值对
func (c *BaseClient) SetKey(key, v string) error {
	conn := c.redisPool.Get()
	defer conn.Close()
	_, err := conn.Do("SET", key, v)
	return err
}

//获得键值对
func (c *BaseClient) GetValue(key string) (string, error) {
	conn := c.redisPool.Get()
	defer conn.Close()
	val, err := redis.String(conn.Do("GET", key))
	if err != nil {
		return "", err
	}
	return val, nil
}

//自增
func (c *BaseClient) StringIncr(key string) (int, error) {
	conn := c.redisPool.Get()
	defer conn.Close()
	return redis.Int(conn.Do("INCR", key))
}

//自减
func (c *BaseClient) StringDecr(key string) (int, error) {
	conn := c.redisPool.Get()
	defer conn.Close()
	return redis.Int(conn.Do("DECR", key))
}

//批量获取
func (c *BaseClient) BatchGet(key ...string) ([]string, error) {
	args := redis.Args{}
	for _, v := range key {
		args = args.Add(v)
	}
	conn := c.redisPool.Get()
	defer conn.Close()
	return redis.Strings(conn.Do("MGET", args...))
}

//批量设置
func (c *BaseClient) MSET(keyAndValue ...string) (string, error) {
	args := redis.Args{}
	for _, v := range keyAndValue {
		args = args.Add(v)
	}

	conn := c.redisPool.Get()

	defer conn.Close()
	return redis.String(conn.Do("MSET", args...))
}

//------------有序集合的操作------------
//有序集合添加元素
func (c *BaseClient) ZADD(key, item string, score interface{}) (bool, error) {
	conn := c.redisPool.Get()
	defer conn.Close()
	return redis.Bool(conn.Do("ZADD", key, score, item))
}

//移除集合元素
func (c *BaseClient) ZREM(key string, item ...string) (bool, error) {
	conn := c.redisPool.Get()
	defer conn.Close()
	args := redis.Args{}.Add(key)
	for _, v := range item {
		args = args.Add(v)
	}
	return redis.Bool(conn.Do("ZREM", args...))
}

//集合内元素数量
func (c *BaseClient) ZCARD(key string) (int, error) {
	conn := c.redisPool.Get()
	defer conn.Close()
	return redis.Int(conn.Do("ZCARD", key))
}

//集合内指定区间分数的成员数
func (c *BaseClient) ZCOUNT(key string, min, max int64) (int, error) {
	conn := c.redisPool.Get()
	defer conn.Close()
	return redis.Int(conn.Do("ZCOUNT", key, min, max))
}

//集合交集
func (c *BaseClient) ZINTERSTORE(destination string, keys ...string) (bool, error) {
	conn := c.redisPool.Get()
	defer conn.Close()
	args := redis.Args{}.Add(destination).Add(len(keys))
	for _, v := range keys {
		args = args.Add(v)
	}
	args = args.Add("AGGREGATE").Add("MIN")
	return redis.Bool(conn.Do("ZINTERSTORE", args...))
}

//集合并集
func (c *BaseClient) ZUNIONSTORE(destination string, keys ...string) (bool, error) {
	conn := c.redisPool.Get()
	defer conn.Close()
	args := redis.Args{}.Add(destination).Add(len(keys))
	for _, v := range keys {
		args = args.Add(v)
	}
	args = args.Add("AGGREGATE").Add("MIN")
	return redis.Bool(conn.Do("ZUNIONSTORE", args...))
}

//获取集合内元素
func (c *BaseClient) ZRANGE(key string, start, end int64, inverted int32) ([]string, error) {
	conn := c.redisPool.Get()
	defer conn.Close()
	command := "ZRANGE"
	if inverted == 1 {
		command = "ZREVRANGE"
	}
	return redis.Strings(conn.Do(command, key, start, end))
}

//获取集合内元素带分数
func (c *BaseClient) ZRANGEWithScores(key string, start, end int64, inverted int32) (map[string]int64, error) {
	conn := c.redisPool.Get()
	defer conn.Close()
	command := "ZRANGE"
	if inverted == 1 {
		command = "ZREVRANGE"
	}
	return redis.Int64Map(conn.Do(command, key, start, end, "WITHSCORES"))
}

//获取集合内元素
func (c *BaseClient) ZRANGEBYSCORE(key string, min, max, offset, count, inverted int64) ([]string, error) {
	conn := c.redisPool.Get()
	defer conn.Close()
	cmin := strconv.Itoa(int(min))
	cmax := strconv.Itoa(int(max))
	if cmin == "0" {
		cmin = "-inf"
	}
	if cmax == "0" {
		cmax = "+inf"
	}
	if inverted == 1 {
		cmin, cmax = cmax, cmin
	}
	command := "ZRANGEBYSCORE"
	if inverted == 1 {
		command = "ZREVRANGEBYSCORE"
	}
	return redis.Strings(conn.Do(command, key, cmin, cmax, "LIMIT", offset, count))
}

//获取集合内元素带分数
func (c *BaseClient) ZRANGEBYSCOREWithScore(key string, min, max, offset, count, inverted int64) (map[string]int64, error) {
	conn := c.redisPool.Get()
	defer conn.Close()
	cmin := strconv.Itoa(int(min))
	cmax := strconv.Itoa(int(max))
	if cmin == "0" {
		cmin = "-inf"
	}
	if cmax == "0" {
		cmax = "+inf"
	}
	if inverted == 1 {
		cmin, cmax = cmax, cmin
	}
	command := "ZRANGEBYSCORE"
	if inverted == 1 {
		command = "ZREVRANGEBYSCORE"
	}
	return redis.Int64Map(conn.Do(command, key, cmin, cmax, "WITHSCORES", "LIMIT", offset, count))
}

//获取集合内元素的分数值，
func (c *BaseClient) ZSCORE(key, item string) (int64, error) {
	conn := c.redisPool.Get()
	defer conn.Close()
	v, err := redis.Int64(conn.Do("ZSCORE", key, item))
	if err == redis.ErrNil {
		return 0, nil
	}
	return v, err
}

//移除有序集中，指定分数（score）区间内的所有成员
func (c *BaseClient) ZREMRANGEBYSCORE(key string, min, max int64) (int64, error) {
	conn := c.redisPool.Get()
	defer conn.Close()
	v, err := redis.Int64(conn.Do("ZREMRANGEBYSCORE", key, min, max))
	if err == redis.ErrNil {
		return 0, nil
	}
	return v, err
}

//返回有序集合中指定成员的排名
func (c *BaseClient) ZRANK(key, item string, inverted int32) (int64, error) {
	conn := c.redisPool.Get()
	defer conn.Close()
	command := "ZRANK"
	if inverted == 1 {
		command = "ZREVRANK"
	}
	v, err := redis.Int64(conn.Do(command, key, item))
	if err == redis.ErrNil {
		return -1, nil
	}
	return v, err
}

//更新集合内元素的分数值，
func (c *BaseClient) ZINCRBY(key, item string, increment int64) (int64, error) {
	conn := c.redisPool.Get()
	defer conn.Close()
	return redis.Int64(conn.Do("ZINCRBY", key, increment, item))
}

//------------list操作------------
//返回list长度
func (c *BaseClient) LLEN(key string) (int64, error) {
	conn := c.redisPool.Get()
	defer conn.Close()
	return redis.Int64(conn.Do("LLEN", key))
}

//按照start ，end遍历list，0表示第一个元素，-1表示最后一个
func (c *BaseClient) LRANGE(key string, start, end int64) ([]string, error) {
	conn := c.redisPool.Get()
	defer conn.Close()
	return redis.Strings(conn.Do("LRANGE", key, start, end))
}

//移除元素，count>0，重头向后搜,count<0，表尾向前搜，count=0，移除所有。返回被移除的个数
func (c *BaseClient) LREM(key string, value interface{}, count int64) (int64, error) {
	conn := c.redisPool.Get()
	defer conn.Close()
	return redis.Int64(conn.Do("LREM", key, count, value))
}

//在尾部插入多个元素。返回插入后的元素个数
func (c *BaseClient) RPUSH(key string, value ...interface{}) (int64, error) {
	conn := c.redisPool.Get()
	defer conn.Close()
	args := redis.Args{}.Add(key)
	for _, v := range value {
		args = args.Add(v)
	}
	return redis.Int64(conn.Do("RPUSH", args...))
}

//在尾部插入多个元素。返回插入后的元素个数
func (c *BaseClient) LPUSH(key string, value ...interface{}) (int64, error) {
	conn := c.redisPool.Get()
	defer conn.Close()
	args := redis.Args{}.Add(key)
	for _, v := range value {
		args = args.Add(v)
	}
	return redis.Int64(conn.Do("LPUSH", args...))
}

//返回索引对应的元素，-1表示最后一个。
func (c *BaseClient) LINDEX(key string, index int64) (string, error) {
	conn := c.redisPool.Get()
	defer conn.Close()
	return redis.String(conn.Do("LINDEX", key, index))
}

//------------hash操作------------
//字段赋值，旧值会被覆盖，设置成功返回1，被覆盖了返回0
func (c *BaseClient) HSET(key, field string, value interface{}) (int, error) {
	conn := c.redisPool.Get()
	defer conn.Close()
	return redis.Int(conn.Do("HSET", key, field, value))
}

//字段赋值，旧值会被覆盖，设置成功返回1，被覆盖了返回0
func (c *BaseClient) HGET(key, field string) (string, error) {
	conn := c.redisPool.Get()
	defer conn.Close()
	v, err := redis.String(conn.Do("HGET", key, field))
	if err == redis.ErrNil {
		return "", nil
	}
	return v, err
}

//命令用于查找所有符合给定模式 pattern 的 key 。。
func (c *BaseClient) Keys(pattern string) ([]string, error) {
	conn := c.redisPool.Get()
	defer conn.Close()

	return redis.Strings(conn.Do("KEYS", pattern))
}

//获取给定多个字段的值
func (c *BaseClient) HMGET(key string, field ...string) ([]string, error) {
	conn := c.redisPool.Get()
	defer conn.Close()
	args := redis.Args{}.Add(key)
	for _, v := range field {
		args = args.Add(v)
	}
	return redis.Strings(conn.Do("HMGET", args...))
}

//设置给定多个字段的值
func (c *BaseClient) HMSET(key string, fieldAndValue ...interface{}) (string, error) {
	conn := c.redisPool.Get()
	defer conn.Close()
	args := redis.Args{}.Add(key)
	for _, v := range fieldAndValue {
		args = args.Add(v)
	}
	return redis.String(conn.Do("HMSET", args...))
}

//通过结构体设置哈希
func (c *BaseClient) HMSETByStruct(key string, dest interface{}) (string, error) {
	conn := c.redisPool.Get()
	defer conn.Close()
	return redis.String(conn.Do("HMSET", redis.Args{}.Add(key).AddFlat(dest)...))
}

//为哈希表 key 中的指定字段的整数值加上增量 increment 。
func (c *BaseClient) HINCRBY(key, field string, increment int) (int, error) {
	conn := c.redisPool.Get()
	defer conn.Close()
	return redis.Int(conn.Do("HINCRBY", key, field, increment))
}

//返回key下所有的字段和值
func (c *BaseClient) HGETALL(key string, dest interface{}) error {
	conn := c.redisPool.Get()
	defer conn.Close()
	value, _ := redis.Values(conn.Do("HGETALL", key))
	return redis.ScanStruct(value, dest)
}

type Converter interface {
	Convert([]interface{}, interface{}) error
}

//批量返回hash
func (c *BaseClient) HGETALLBatch(convert Converter, dest interface{}, key ...string) error {
	luaScript := `local rst={}; for i,v in pairs(KEYS) do rst[i]=redis.call('HGETALL', v) end;return rst`
	conn := c.redisPool.Get()
	defer conn.Close()
	args := redis.Args{}
	lua := redis.NewScript(len(key), luaScript)
	for _, v := range key {
		args = args.Add(v)
	}
	v, err := redis.Values(lua.Do(conn, args...))
	if err != nil {
		return err
	}
	return convert.Convert(v, dest)
}

func GetRedisPool(isTest, isCluster int) (*BaseClient, error) {
	var Address string
	if isTest == 1 && isCluster == 1 {
		Address = AddrInternalTest
	} else if isTest == 1 && isCluster == 0 {
		Address = AddrExternalTest
	} else if isTest == 0 && isCluster == 1 {
		Address = AddrInternalProd
	} else if isTest == 0 && isCluster == 0 {
		Address = AddrExternalProd
	}
	pool := getRedisPool(Address)
	return &BaseClient{redisPool: pool, address: Address}, nil
}

func GetBudRedisKey(business, uid string, others ...string) string {
	other := ""
	for _, avg := range others {
		other += "_" + avg
	}
	return fmt.Sprintf("%s:%s%s", business, uid, other)
}
