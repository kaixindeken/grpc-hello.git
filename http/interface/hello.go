package main

import (
	"context"
	"encoding/json"
	"fmt"
	"gitee.com/kaixindeken/grpc-hello/proto/project"
	"google.golang.org/grpc/grpclog"
	"net/http"
	"time"
)

func hello(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Access-Control-Allow-Origin", "*")
	w.Header().Set("Access-Control-Allow-Headers", "content-type")

	rs := Result{
		Result: 0,
	}
	defer func() {
		if err := json.NewEncoder(w).Encode(rs); err != nil {
			grpclog.Error(err)
		}
		grpclog.Info("rs", rs)
	}()
	if r.Method == "GET" {
		c := project.NewHelloServiceClient(conn)
		ctx, cancel := context.WithTimeout(context.Background(), time.Second)
		defer cancel()
		rsp, err := c.SayHello(ctx, &project.Hello{Hello: "你好"})
		if err != nil {
			fmt.Println("hello err")
			rs.Result = 10001
			rs.Msg = "无法连接服务器"
			return
		}
		if rsp != nil {
			rs.Msg = rsp.Hi
		}
	}
}
