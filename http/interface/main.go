package main

import (
	"fmt"
	"google.golang.org/grpc"
	"net/http"
)

var conn *grpc.ClientConn

func main() {
	var err error
	conn, err = grpc.Dial("localhost:13456", grpc.WithInsecure())
	defer conn.Close()
	if err != nil {
		fmt.Println("did not connect: ", err)
	}

	http.HandleFunc("/hello", hello)

	err = http.ListenAndServe(":13232", nil)
	if err != nil {
		fmt.Println("ListenAndServe:", err)
	}
}
