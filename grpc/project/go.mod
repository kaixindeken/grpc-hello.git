module gitee.com/kaixindeken/grpc-hello/grpc/project

go 1.17

require (
	gitee.com/kaixindeken/grpc-hello/domain/redis v0.0.0-20211110083112-c1ade16ec36a
	gitee.com/kaixindeken/grpc-hello/proto/project v0.0.0-20211110083112-c1ade16ec36a
	google.golang.org/grpc v1.42.0
)

require (
	github.com/golang/protobuf v1.5.2 // indirect
	github.com/gomodule/redigo v1.8.5 // indirect
	golang.org/x/net v0.0.0-20211104170005-ce137452f963 // indirect
	golang.org/x/sys v0.0.0-20210423082822-04245dca01da // indirect
	golang.org/x/text v0.3.6 // indirect
	google.golang.org/genproto v0.0.0-20200526211855-cb27e3aa2013 // indirect
	google.golang.org/protobuf v1.26.0 // indirect
)
