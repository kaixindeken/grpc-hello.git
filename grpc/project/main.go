package main

import (
	"fmt"
	"gitee.com/kaixindeken/grpc-hello/domain/redis"
	"gitee.com/kaixindeken/grpc-hello/proto/project"
	"google.golang.org/grpc"
	"google.golang.org/grpc/reflection"
	"log"
	"net"
)

const (
	port = ":13456"
)

var (
	testRedis *redis.BaseClient
)

type server struct{}

func main() {
	var err error
	//redis
	testRedis, err = redis.GetRedisPool(1, 1)
	if err != nil {
		fmt.Println("redis err:", err.Error())
		return
	}
	//grpc
	lis, err := net.Listen("tcp", port)
	if err != nil {
		log.Println("failed to listen: ", err.Error())
	}
	s := grpc.NewServer()
	project.RegisterHelloServiceServer(s, &server{})
	reflection.Register(s)
	if err := s.Serve(lis); err != nil {
		log.Println("failed to serve:", err.Error())
	}
}
